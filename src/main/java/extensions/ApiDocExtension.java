package extensions;

/**
 * Created by wb on 2018/11/28.
 */
public class ApiDocExtension {
    private String controllerPath;

    public String getControllerPath() {
        return controllerPath;
    }

    public void setControllerPath(String controllerPath) {
        this.controllerPath = controllerPath;
    }
}